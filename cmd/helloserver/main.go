package main

import (
	"net/http"

	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type SessionManger struct {
	cookieName string
	lock sync.Mutex
	provider Provider
	maxAge int64
}

func NewManager(provideName, cookieName string, maxAge int64) (*Manager, error) {
	provider, ok := provides[provideName]

	if !ok {

	}
}

func main() {
	logrus.SetFormatter(&logrus.JSONFormatter{})

	r := mux.NewRouter()

	r.HandleFunc("/", RenderRootAuthForm)
	r.HandleFunc("/greet", RenderGreeting)
	r.HandleFunc("/error", RenderErrorPlaceholder)
	r.HandleFunc("/notfound", Render404)

	r.HandleFunc("/greet/update", UpdateGreeting).Methods("POST")
	r.HandleFunc("/auth/signout", SignOutUser).Methods("POST")
	r.HandleFunc("/auth/password/reset", InitPasswordReset).Methods("POST")
	r.HandleFunc("/auth/login", TryUserSignIn).Methods("POST")
	r.HandleFunc("/auth/newaccount", CreateNewAccount).Methods("POST")
	r.HandleFunc("/auth/email/validate", VerifyEmailExists).Methods("POST")
	r.HandleFunc("/auth/email/challenge", SendOneTimeCode).Methods("POST")
	r.HandleFunc("/auth/email/response", ValidateOneTimeCode).Methods("POST")

	CSRF := csrf.Protect([]byte("32-byte-long-aauth-key"))(r)

	http.ListenAndServeTLS(":8000", )
}

func RenderRootAuthForm(w http.ResponseWriter, r *http.Request) {

}
