package logger

import (
	cfg "securehelloservice/internal/config"

	"os"
	"reflect"
	"strings"

	"github.com/sirupsen/logrus"
)

const logFileVariable = "HELLOSVC_LOGFILE"
const logFileFallback = "/var/log/hellosvc"
const logFilePermissions = 0644
const logFileMode = os.O_CREATE | os.O_APPEND | os.O_WRONLY

var logger Logger

func LogService() Logger {
	return logger
}

type Logger struct {
	ctx *logrus.Entry
}

func init() {
	envData := cfg.GetEnvironmentVariables()
	logrus.SetFormatter(&logrus.JSONFormatter{})
	var loggingContext *logrus.Entry

	r := reflect.ValueOf(&envData).Elem()
	fields := reflect.TypeOf(envData)
	values := reflect.ValueOf(envData)

	for i := 0; i < r.NumField(); i++ {
		n := fields.Field(i).Name
		v := values.Field(i)

		loggingContext = logrus.WithField(n, v)
	}

	logVariable, envError := os.LookupEnv(logFileVariable)

	if envError {
		logVariable = logFileFallback
	}

	logFile, fileError := os.OpenFile(logVariable, logFileMode, logFilePermissions)

	if fileError != nil {
		panic(fileError)
	}

	logrus.SetOutput(logFile)
	logrus.RegisterExitHandler(func() {
		if logFile == nil {
			return
		}

		logFile.Close()
	})

	logger = Logger{loggingContext}
}

func getLogLevel(envValue string) logrus.Level {
	switch strings.ToUpper(envValue) {
	case "WARN":
		return logrus.WarnLevel
	case "ERROR":
		return logrus.ErrorLevel
	case "FATAL":
		return logrus.FatalLevel
	case "DEBUG":
		return logrus.DebugLevel
	case "INFO":
		return logrus.InfoLevel
	default:
		return logrus.WarnLevel
	}
}
