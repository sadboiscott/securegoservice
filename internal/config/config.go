package config

import (
	"os"
	"os/user"
	"strconv"
)

const HostnameVariable string = "HELLOSVC_HOSTNAME"
const HostnameFallback string = "localhost"

const PortVariable string = "HELLOSVC_PORT"
const PortVariableFallback int64 = 60001

const LogLevelVariable string = "HELLO_LOG_LEVEL"
const LogFallback string = "WARN"

const SudoUserVariable string = "SUDO_USER"

type EnvironmentVariables struct {
	Hostname    string
	PortNumber  int64
	LogLevel    string
	ProcessUser string
	SudoUser    string
	ProcessId   int
}

func GetEnvironmentVariables() EnvironmentVariables {
	user, userErr := user.Current()
	procUser := "Unknown"

	if userErr == nil {
		procUser = user.Name
	}

	sudoUser := os.Getenv(SudoUserVariable)
	pid := os.Getpid()
	envValues := EnvironmentVariables{HostnameFallback, PortVariableFallback, LogFallback, procUser, sudoUser, pid}

	host, hostExists := os.LookupEnv(HostnameVariable)

	if hostExists {
		envValues.Hostname = host
	}

	port, portExists := os.LookupEnv(PortVariable)

	if portExists {
		portNo, parseErr := strconv.ParseInt(port, 0, 32)

		if parseErr != nil || portNo < 1 || portNo > 65535 {
			envValues.PortNumber = portNo
		}
	}

	level, levelExists := os.LookupEnv(LogLevelVariable)

	if levelExists {
		envValues.LogLevel = level
	}

	return envValues
}
